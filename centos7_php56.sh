#!/usr/bin/env bash
rpm -Uvh http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-9.noarch.rpm
rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
yum update -y
yum install -y yum-utils nano net-tools
yum-config-manager --enable remi
yum-config-manager --enable remi-php56
yum update -y
yum install -y php php-cli php-common php-fpm php-mbstring php-mysql php-pdo php-xml php-pgsql