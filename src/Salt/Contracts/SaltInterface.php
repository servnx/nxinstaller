<?php

namespace Salt\Contracts;

interface SaltInterface
{
    public function config();

    public function tools();

    /**
     * Executes the given module with given params and given data
     *
     * @param string $target
     * @param string $module
     * @param array $params
     * @param array $data
     * @return array|mixed
     */
    public function execute($target, $module, $params = [], $data = []);

    /**
     * Pings the target
     *
     * @param $target
     * @return mixed
     */
    public function ping($target);

    public function osFamily($target);

    public function getKeys($list = 'all');

    public function acceptMinionKey($target);

    /**
     * Returns the given key from the results.
     *
     * @param $key
     * @return mixed
     */
    public function getResults($key = null);

    /**
     * Resets properties
     */
    public function clean();
}