<?php

namespace NxInstaller;

use NxInstaller\Classes\SaltCli;
use PHPUnit\Framework\TestCase;
use Salt\SaltConfig;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class BaseTestCase extends TestCase
{
    protected $input;
    protected $output;
    protected $io;
    protected $salt;
    protected $config;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->input = new ArgvInput();
        $this->output = new ConsoleOutput();

        $this->io = new SymfonyStyle($this->input, $this->output);
        $this->config = new SaltConfig(__DIR__ . '/../src/NxInstaller/Configs');
        $this->salt = new SaltCli($this->io, $this->config);
    }

    protected function getOsConfig($path)
    {
        $delim = $this->config->getDelimiter();

        $os = strtolower($this->salt->os(hostname()));

        return $this->config->get('os' . $delim . $os . $delim . $path);
    }

    protected function setOsConfig($path, $value)
    {
        $delim = $this->config->getDelimiter();

        $os = strtolower($this->salt->os(hostname()));

        $this->config->set('os' . $delim . $os . $delim . $path, $value);
    }
}