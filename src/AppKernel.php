<?php

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        // When you install a third-party bundle or create a new bundle in your
        // application, you must add it in the following array to register it
        // in the application. Otherwise, the bundle won't be enabled and you
        // won't be able to use it.
        $bundles = [

        ];

        // Some bundles are only used while developing the application or during
        // the unit and functional tests. Therefore, they are only registered
        // when the application runs in 'dev' or 'test' environments. This allows
        // to increase application performance in the production environment.
        if (in_array($this->getEnvironment(), ['dev', 'test'])) {


            if ('test' === $this->getEnvironment()) {

            }
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__ . '/NxInstaller/Resources/config/services.php');
    }
}