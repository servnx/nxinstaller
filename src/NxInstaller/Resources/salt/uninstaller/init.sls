# todo: We really should map these package names based on OS

remove_composer:
  pkg.purged:
    - name: composer

remove_crontab:
  cron.absent:
    - name: /usr/bin/php /var/www/nxpanel/artisan schedule:run >> /dev/null 2>&1

remove_mysql_server:
  pkg.purged:
    - name: mysql-server

remove_mysql_client:
  pkg.purged:
    - name: mysql-client

remove_mariadb_client:
  pkg.purged:
    - name: mariadb-client

remove_mariadb_server:
  pkg.purged:
    - name: mariadb-server

remove_postgres:
  pkg.purged:
    - name: postgresql

remove_nginx:
  pkg.purged:
    - name: nginx

remove_node:
  pkg.purged:
    - name: nodejs

remove_npm:
  pkg.purged:
    - name: npm

remove_supervisor:
  pkg.purged:
    - name: supervisor

remove_repo:
  cmd.run:
    - name: rm -rf /var/www/nxpanel

remove_user_nxpanel:
  user.absent:
    - name: nxpanel