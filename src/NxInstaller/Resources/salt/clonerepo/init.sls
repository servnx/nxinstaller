nxpanel-clone:
  git.latest:
    - name: https://poppabear@bitbucket.org/servnx/nxpanel.git
    - target: /var/www/nxpanel
    - rev: master
    - branch: master
    - force_reset: True
    - force_clone: True