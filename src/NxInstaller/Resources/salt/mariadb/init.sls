mariadb-client-server:
  pkg.installed:
      - pkgs:
        - mariadb-client
        - mariadb-server

  service.running:
    - name: mariadb-server
    - reload: True
    - enable: True

  service.running:
    -name: mariadb-client
    - reload: True
    - enable: True

#mariadb-dbnxpanel-db:
#  mysql_database.present:
#    - name: dbnxpanel
#    - require:
#      - pkg: mysql-server
#
#mysql-dbnxpanel-user:
#  mysql_user.present:
#    - name: dbnxpanel
#    - host: '%'
#    - password: {{ pillar['password'] }}
#    - require:
#          - pkg: mysql-server
#    - saltenv:
#      - LC_ALL: "en_US.utf8"
#
#mysql-dbnxpanel-grants:
#  mysql_grants.present:
#    - grant: all privileges
#    - database: dbnxpanel.*
#    - user: dbnxpanel