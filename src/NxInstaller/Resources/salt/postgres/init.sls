postgresql:
  pkg.installed:
    - name: postgresql

  service.running:
      - name: postgresql
      - reload: True
      - enable: True

postgres-dbnxpanel-user:
  postgres_user.present:
    - name: dbnxpanel
    - password: {{ pillar['password'] }}
    - require:
          - pkg: postgresql

postgres-dbnxpanel-db:
  postgres_database.present:
    - name: dbnxpanel
    - owner: dbnxpanel
    - require:
      - pkg: postgresql