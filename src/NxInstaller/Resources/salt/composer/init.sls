composer:
  pkg.installed:
    - name: composer

/var/www/nxpanel:
  composer.installed:
    - composer: /usr/bin/composer
    - php: /usr/bin/php
    - no_dev: true
    - user: nxpanel
    - require:
      - pkg: composer