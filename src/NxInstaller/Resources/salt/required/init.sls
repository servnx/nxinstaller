#openssh-client:
#  pkg.installed:
#    - name: openssh-client

openssh-server:
  pkg.installed:
    - name: openssh-server

openssl:
  pkg.installed:
    - name: openssl

sed:
  pkg.installed:
    - name: sed

zip:
  pkg.installed:
    - name: zip

unzip:
  pkg.installed:
    - name: unzip

curl:
  pkg.installed:
    - name: curl

git:
  pkg.installed:
    - name: git

#php-mbstring:
#  pkg.installed:
#    - name: php-mbstring
#
#php-dom:
#  pkg.installed:
#    - name: php-dom
#
#php-mysql:
#  pkg.installed:
#    - name: php-mysql
#
#php-pgsql:
#  pkg.installed:
#    - name: php-pgsql
#
#php-xml:
#   pkg.installed:
#     - name: php-xml