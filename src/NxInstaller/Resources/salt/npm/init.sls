npm:
  pkg.installed:
    - name: npm

/var/www/nxpanel:
  npm.bootstrap:
    - user: nxpanel
    - require:
      - pkg: npm

install_gulp_globally:
  cmd.run:
    - name: npm install -g gulp
    - required:
      - pkg: npm