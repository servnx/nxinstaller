<?php
/**
 * Symfony Components
 */
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * NxInstaller
 */
use NxInstaller\Classes\SaltCli;

/**
 * Additional Imports
 */
use Salt\SaltConfig;

// register the event dispatcher service
$container->setDefinition('event_dispatcher', new Definition(
    EventDispatcher::class
));

/*
$container->setDefinition('input', new Definition(
    ArgvInput::class
));

$container->setDefinition('output', new Definition(
    ConsoleOutput::class
));

$container->setDefinition('config', new Definition(
    SaltConfig::class,
    [
        __DIR__ . '/../../Configs'
    ]
));


$definition = new Definition(SaltCli::class);
$definition->setAutowired(true);

$container->setDefinition('saltcli', $definition);*/