users:

  nxpanel:
    fullname: NxPanel User
    password: '' # dynamically set

    shell: /bin/bash

    sudouser: True
    sudo_rules:
      - ALL=(ALL) NOPASSWD:ALL

    sudo_defaults:
      - '!requiretty'

    groups:
      - www-data