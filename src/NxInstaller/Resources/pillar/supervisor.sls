supervisor:
  programs:
    nxpanel-worker:
      process_name: '%(program_name)s_%(process_num)02d'
      autorestart: 'true'
      autostart: 'true'
      redirect_stderr: 'true'
      startsecs: '5'
      stdout_logfile_maxbytes: '50MB'
      stdout_logfile_backups: '10'
      command: php /var/www/nxpanel/artisan queue:work database --sleep=3 --tries=3
      user: 'nxpanel'
      numprocs: '8'
      startretries: '10'
      stdout_logfile: '/var/www/nxpanel/worker.log'

#[program:nxpanel-worker]
#process_name=%(program_name)s_%(process_num)02d
#command=php /var/www/nxpanel/artisan queue:work database --sleep=3 --tries=3
#autostart=true
#autorestart=true
#user=nxpanel
#numprocs=8
#redirect_stderr=true
#stdout_logfile=/var/www/nxpanel/worker.log