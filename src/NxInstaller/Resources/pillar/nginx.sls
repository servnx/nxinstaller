nginx:
  ng:

    install_from_ppa: True

    ppa_version: 'stable'

    from_source: False

    service:
      enable: True

    server:

      # nginx.conf (main server) declarations
      # dictionaries map to blocks {} and lists cause the same declaration to repeat with different values
      config:
        user: www-data
        worker_processes: 20
        error_log: /var/log/nginx/error.log
        pid: /run/nginx.pid

        events:
          worker_connections: 1024
          multi_accept: 'on'

        http:
          access_log: /var/log/nginx/access.log
          sendfile: 'on'
          keepalive_timeout: '65'
          default_type: application/octet-stream
          types_hash_max_size: '2048'
          tcp_nodelay: 'on'
          gzip: 'on'
          gzip_disable: 'msie6'

          include:
            - /etc/nginx/mime.types
            - /etc/nginx/conf.d/*.conf
            - /etc/nginx/sites-enabled/*

    servers:

      managed:

        main.conf:

          enabled: True
          overwrite: True

          config:
            - server:

              - server_name: nxpanel.app

              - root:
                - /var/www/nxpanel

              - listen:
                - 8000

              - index:
                - index.html
                - index.php
                - index.htm

              - location /:
                - try_files:
                  - /public/$uri
                  - /public/$uri/
                  - /public/index.php?$query_string

              - location ~* \.php$:
                - try_files:
                  - $uri
                  - =404

                - include:
                  - /etc/nginx/fastcgi_params

                - fastcgi_split_path_info:
                  - ^(.+\.php)(/.+)$

                - fastcgi_pass:
                  - unix:/var/run/php/php7.0-fpm.sock

                - fastcgi_index:
                  - index.php

                - fastcgi_param:
                  - SCRIPT_FILENAME
                  - $document_root$fastcgi_script_name

                - index:
                  - fastcgi_params

#server {
#    server_name nxpanel.dev;
#    root /var/www/nxpanel;
#    listen 8000;
#    index index.html index.php index.htm;
#
#    location / {
#        try_files /public/$uri /public/$uri/ /public/index.php?$query_string;
#    }
#
#    location ~* \.php$ {
#        try_files $uri =404;
#        include /etc/nginx/fastcgi_params;
#        fastcgi_split_path_info ^(.+\.php)(/.+)$;
#        fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
#        fastcgi_index index.php;
#        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
#        fastcgi_intercept_errors on;
#    }
#}