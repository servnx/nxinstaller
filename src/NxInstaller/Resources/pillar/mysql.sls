mysql:
  global:
    client-server:
      default_character_set: utf8

  clients:
    mysql:
      default_character_set: utf8
    mysqldump:
      default_character_set: utf8

  library:
    client:
      default_character_set: utf8

  server:
    # Use this account for database admin (defaults to root)
    root_user: 'root'
#
#    # root_password: '' - to have root@localhost without password
    root_password: ''
#    root_password_hash: ''

    user: mysql

    # If you only manage the dbs and users and the server is on
    # another host
    #host: 123.123.123.123

    # my.cnf sections changes
    mysqld:

      # you can use either underscore or hyphen in param names
#      bind-address: 0.0.0.0
#      log_bin: /var/log/mysql/mysql-bin.log
#      datadir: /var/lib/mysql
      port: 3306
#      binlog_do_db: dbnxpanel
#      auto_increment_increment: 5
#      binlog-ignore-db:
#       - mysql
#       - sys
#       - information_schema
#       - performance_schema


    mysql:
      # my.cnf param that not require value
      no-auto-rehash: noarg_present

#  salt_user:
#    salt_user_name: 'root'
#    salt_user_password: '' # todo: dynamically set
#    grants:
#      - 'all privileges'

  # Manage databases
  database:
    - dbnxpanel

  schema:
    dbnxpanel:
      load: False

  # Manage users
  # you can get pillar for existing server using scripts/import_users.py script
  user:
    dbnxpanel:
      password: '' # todo: dynamically set
      host: localhost

      databases:
        - database: dbnxpanel
          grants: ['all privileges']

  # Install MySQL headers
  dev:
    # Install dev package - defaults to False
    install: False

