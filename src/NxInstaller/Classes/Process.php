<?php

namespace NxInstaller\Classes;

use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process as SymfonyProcess;

use Salt\Utilities\Process as SaltProcess;

class Process extends SaltProcess
{

    /**
     * @var SymfonyStyle $io
     */
    private $io;

    /**
     * @var string $title
     */
    private $title;

    /**
     * @var bool $progressDisabled
     */
    private $progressDisabled = false;

    /**
     * Process constructor.
     * @param SymfonyStyle $io
     */
    public function __construct(SymfonyStyle $io)
    {
        $this->io = $io;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Executes a new Process instance
     *
     * @param string $cmd
     * @return string
     */
    public function execute($cmd)
    {
        if (!$this->io->isDecorated()) {
            $this->progressDisabled = true;
        }

        $this->io->writeln("<fg=cyan>$this->title</>");

        $process = new SymfonyProcess($cmd);

        $bar = null;

        if (!$this->progressDisabled) {
            $bar = $this->progressStart();
        }

        $this->processStart($process, $bar);

        if (!$process->isSuccessful()) {
            $method = $this->failureLevel;
            $this->$method($process);
        }

        if (!$this->progressDisabled) {
            $this->progressStop($bar);
        }

        $this->processStop($process);

        echo "\n\n";
        return $process->getOutput();
    }


    /**
     * @return bool
     */
    public function isProgressDisabled()
    {
        return $this->progressDisabled;
    }

    /**
     * @param bool $progressDisabled
     * @return $this
     */
    public function setProgressDisabled($progressDisabled)
    {
        $this->progressDisabled = $progressDisabled;
        return $this;
    }

    /**
     * @return ProgressBar
     */
    protected function progressStart()
    {
        $bar = $this->io->createProgressBar();
        $bar->setBarCharacter('<fg=magenta>=</>');
        $bar->setFormat('[%bar%] (<fg=cyan>%message%</>)');
        $bar->setMessage('Please Wait ...');
        //$bar->setRedrawFrequency(20); todo: may be useful for platforms like CentOS
        $bar->start();

        return $bar;
    }

    /**
     * @param $bar
     */
    protected function progressStop(ProgressBar $bar)
    {
        $bar->setMessage("<fg=green>Done!</>");
        $bar->finish();
    }

    /**
     * @param SymfonyProcess $process
     * @param ProgressBar $bar
     * @throws \Exception
     */
    protected function processStart(SymfonyProcess $process, ProgressBar $bar = null)
    {
        if ($bar == null && !$this->progressDisabled) {
            throw new \Exception('ProgressBar is null. setProgressDisabled if you want to disable ProgressBar');
        }

        $process->setTimeout(3600);

        if (!$this->progressDisabled) {
            $process->start();

            while ($process->isRunning()) {
                $this->progressAdvance($bar);
            }
        } else {
            $process->run();
        }
    }

    /**
     * @param $bar
     */
    protected function progressAdvance(ProgressBar $bar)
    {
        $bar->advance();
        usleep(200000);
    }
}