<?php

namespace NxInstaller\Classes;

use NxInstaller\Traits\ConsoleTools;

use Symfony\Component\Console\Style\SymfonyStyle;

use Salt\Salt;
use Salt\SaltConfig;

class SaltCli extends Salt
{
    use ConsoleTools;

    /**
     * @var SymfonyStyle $io
     */
    private $io;

    public function __construct(SymfonyStyle $io, SaltConfig $config = null)
    {
        parent::__construct($config);

        $this->io = $io;
    }

    protected function cmd($cmd)
    {
        $a = explode(' ', $cmd);

        if ($a[0] == 'salt') {
            $title = "Executing '" . $a[2] . "' " . $a[3] . " on " . $a[1] . " ...";
        } else {
            $title = "Executing '" . $a[0] . "' " . $a[1] . " ...";
        }

        return (new Process($this->io))
            ->setTitle($title)
            ->execute($cmd);
    }

    protected function validateResults()
    {
        // we do this because the process sometimes returns as successful, but Salt returns with errors
        if (is_array($this->results)) {
            foreach ($this->results as $t) {
                if (is_array($t)) {
                    $results = $this->getResults('result');
                    $comments = $this->getResults('comment');

                    foreach ($results as $true) {
                        if (!$true) {
                            $this->throwError($comments);
                        }
                    }

                } elseif (is_string($t)) {
                    $this->io->writeln("<bg=yellow;fg=black>[Warning] $t</>\n");
                }
            }
        }
    }
}