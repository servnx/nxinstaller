<?php

namespace NxInstaller\Installer;

use Symfony\Component\Console\Style\SymfonyStyle;

use Salt\Salt;
use Salt\SaltConfig;

abstract class BaseInstaller
{
    /**
     * @var SymfonyStyle $io
     */
    protected $io;

    /**
     * @var Salt
     */
    protected $salt;

    /**
     * @var SaltConfig $config
     */
    protected $config;

    /**
     * @var int $timeout
     */
    protected $timeout = 15;

    public function __construct(SymfonyStyle $io, Salt $salt)
    {
        $this->io = $io;
        $this->salt = $salt;
        $this->config = $salt->config();
    }

    abstract public function handle();

    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
        return $this;
    }

    protected function head($text)
    {
        if ($text !== null) {
            $this->io->writeln("\n<fg=black;bg=white>" . str_repeat('=', 80));
            $this->io->writeln('    ' . $text . str_repeat(' ', (76 - strlen($text))));
            $this->io->writeln(str_repeat('=', 80) . "</>\n");
        }
    }

    protected function done()
    {
        $this->io->writeln("\n<fg=black;bg=green>[OK] Done!" . str_repeat(' ', 70) . "</>");
    }

    protected function warning($text)
    {
        $this->io->writeln("<bg=yellow;fg=black>[Warning] $text</>\n");
    }

    protected function stub($filename)
    {
        $delim = $this->config->getDelimiter();
        return $this->config->get('app' . $delim . 'stub_dir') . "/$filename";
    }

    protected function getOsConfig($path)
    {
        $delim = $this->config->getDelimiter();

        $os = strtolower($this->salt->os(hostname()));

        return $this->config->get('os' . $delim . $os . $delim . $path);
    }

    protected function setOsConfig($path, $value)
    {
        $delim = $this->config->getDelimiter();

        $os = strtolower($this->salt->os(hostname()));

        $this->config->set('os' . $delim . $os . $delim . $path, $value);
    }


}