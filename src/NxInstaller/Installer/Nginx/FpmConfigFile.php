<?php

namespace NxInstaller\Installer\Nginx;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class FpmConfigFile extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $site = $this->config->get('app' . $delim . 'hostname');

        $pool_path = $this->getOsConfig('php-fpm-pool_path');
        $sock_path = $this->getOsConfig('php-fpm-sock_path');
        $listen_user = $this->getOsConfig('php-fpm-listen_user');

        if (file_exists("$pool_path/$site.conf")) {
            $this->warning("$pool_path/$site.conf already exists ...");

            (new Process($this->io))
                ->setTitle("Removing $pool_path/$site.conf ...")
                ->execute("rm $pool_path/$site.conf");

        }

        $this->io->writeln("<fg=magenta>Generating $pool_path/$site.conf ....</>");

        $find = [
            'DummySite',
            'DummyUser',
            'DummySockPath',
            'DummyListen'
        ];

        $replace = [
            $site,
            'nxpanel',
            $sock_path,
            $listen_user
        ];

        $file = file_get_contents($this->stub('php-fpm-ondemand.stub'));
        $contents = str_replace($find, $replace, $file);

        file_put_contents("$pool_path/$site.conf", $contents);

        if (!file_exists("$pool_path/$site.conf")) {
            throw new \Exception("'$site.conf' file failed to generate.");
        }
    }
}