<?php

namespace NxInstaller\Installer\Nginx;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class NginxState extends BaseInstaller
{
    public function handle($param = null)
    {
        $target = $param;

        if ($target === null) {
            $target = hostname();
        }

        $delim = $this->config->getDelimiter();

        $site = $this->config->get('app' . $delim . 'hostname');

        $os = strtolower($this->salt->os(hostname()));
        $listen_user = $this->getOsConfig('php-fpm-listen_user');
        $sock_path = $this->getOsConfig('php-fpm-sock_path');

        $this->config->setPillar(
            'nginx',
            'user',
            $listen_user
        );

        $this->config->setPillar(
            'nginx',
            'fastcgi_pass',
            "unix:$sock_path/$site.sock"
        );

        $this->config->setPillar(
            'nginx',
            'server_name',
            $site
        );

        $data = $this->config->getPillar('nginx');

        $this->salt->execute($target, 'state.sls', ['nginx.ng'], $data);

        if (!is_dir('/var/www')) {
            (new Process($this->io))
                ->setTitle("Creating /var/www")
                ->execute("mkdir /var/www && chmod -R 755 /var/www");
        }
    }
}