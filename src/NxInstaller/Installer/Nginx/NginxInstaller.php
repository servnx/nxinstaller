<?php

namespace NxInstaller\Installer\Nginx;

use NxInstaller\Installer\BaseInstaller;

class NginxInstaller extends BaseInstaller
{
    private $steps = [
        FpmConfigFile::class,
        NginxState::class
    ];

    public function handle()
    {
        foreach ($this->steps as $class) {
            (new $class($this->io, $this->salt))->handle();
        }
    }
}