<?php

namespace NxInstaller\Installer\Prerequisites;

use NxInstaller\Installer\BaseInstaller;

class Required extends BaseInstaller
{
    public function handle($param = null)
    {
        $target = $param;

        if ($target === null) {
            $target = hostname();
        }

        $this->salt->execute($target, 'state.sls', ['required']);
    }
}