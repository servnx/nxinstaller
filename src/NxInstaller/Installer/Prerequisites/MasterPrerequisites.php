<?php

namespace NxInstaller\Installer\Prerequisites;

use NxInstaller\Installer\BaseInstaller;

class MasterPrerequisites extends BaseInstaller
{
    private $steps = [
        Required::class,
        NodeJS::class,
    ];

    public function handle()
    {
        foreach ($this->steps as $class) {
            (new $class($this->io, $this->salt))->handle();
        }
    }

}