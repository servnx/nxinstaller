<?php

namespace NxInstaller\Installer\NxPanel;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class InitJsonFile extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $path = $this->config->get('app' . $delim . 'install_dir');

        file_put_contents("$path/init.json", json_encode($this->config->get('app')));

        (new Process($this->io))
            ->setTitle("Setting owner:group on $path/init.json")
            ->execute("chown nxpanel:nxpanel $path/init.json");

        (new Process($this->io))
            ->setTitle("Setting permissions on $path/init.json")
            ->execute("chmod 750 $path/init.json");
    }
}