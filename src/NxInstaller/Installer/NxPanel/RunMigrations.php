<?php

namespace NxInstaller\Installer\NxPanel;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class RunMigrations extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $path = $this->config->get('app' . $delim . 'install_dir');

        (new Process($this->io))
            ->setTitle("Running migrations ...")
            ->execute("su -c 'cd $path && php artisan migrate --force' - nxpanel");
    }
}