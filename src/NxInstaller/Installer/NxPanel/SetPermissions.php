<?php

namespace NxInstaller\Installer\NxPanel;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class SetPermissions extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $install_dir = $this->config->get('app' . $delim . 'install_dir');

        $commands = [
            "chown -R nxpanel:nxpanel $install_dir",
            "chmod 750 $install_dir/artisan",
            "chmod 640 $install_dir/.env"
        ];

        foreach ($commands as $command) {
            (new Process($this->io))
                ->setTitle("Executing $command ...")
                ->execute($command);
        }
    }
}