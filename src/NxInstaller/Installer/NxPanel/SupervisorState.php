<?php

namespace NxInstaller\Installer\NxPanel;

use NxInstaller\Installer\BaseInstaller;

class SupervisorState extends BaseInstaller
{
    public function handle($param = null)
    {
        $target = $param;

        if ($target === null) {
            $target = hostname();
        }

        $this->salt->execute($target, 'state.sls', ['supervisor']);
    }
}