<?php

namespace NxInstaller\Installer\NxPanel;

use NxInstaller\Installer\BaseInstaller;

class CronjobState extends BaseInstaller
{
    public function handle($param = null)
    {
        $target = $param;

        if ($target === null) {
            $target = hostname();
        }

        $this->salt->execute($target, 'state.sls', ['cronjob']);
    }
}