<?php

namespace NxInstaller\Installer\NxPanel;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class EnvFile extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $install_dir = $this->config->get('app' . $delim . 'install_dir');

        if (file_exists("$install_dir/.env")) {
            $this->warning("$install_dir/.env already exists!");

            (new Process($this->io))
                ->setTitle("Removing $install_dir/.env ...")
                ->execute("rm $install_dir/.env");
        }

        $find = [
            'DummyDomain',
            'DummyDriverDB',
            'DummyPasswordDB',
        ];

        $replace = [
            $this->config->get('app' . $delim . 'hostname'),
            strtolower($this->config->get('app' . $delim . 'database_driver')),
            $this->config->get('app' . $delim . 'database_password'),
        ];

        $stub_dir = $this->config->get('app' . $delim . 'stub_dir');

        $file = file_get_contents("$stub_dir/.env.stub");

        if ($file === false) {
            throw new \Exception("'$stub_dir/.env.stub' error getting file contents. Please report this bug.");
        }

        $contents = str_replace($find, $replace, $file);

        file_put_contents("$install_dir/.env", $contents);

        if (!file_exists("$install_dir/.env")) {
            throw new \Exception("'$install_dir/.env' file failed to generate. Please report this bug.");
        }
    }
}