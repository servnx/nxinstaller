<?php

namespace NxInstaller\Installer\NxPanel;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class NxPanelInstaller extends BaseInstaller
{
    private $steps = [
        AdminPassword::class,
        CloneRepository::class,
        EnvFile::class,
        SetPermissions::class,
        ComposerState::class,
        NpmState::class,
        AppKey::class,
        CronjobState::class,
        SupervisorState::class,
        InitJsonFile::class,
        RunMigrations::class,
        InitializeNxpanel::class,
        VendorPublish::class
    ];

    public function handle()
    {
        foreach ($this->steps as $class) {
            (new $class($this->io, $this->salt))->handle();
        }
    }

}