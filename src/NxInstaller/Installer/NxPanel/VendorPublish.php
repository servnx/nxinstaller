<?php

namespace NxInstaller\Installer\NxPanel;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class VendorPublish extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $path = $this->config->get('app' . $delim . 'install_dir');

        (new Process($this->io))
            ->setTitle("Publishing Assets ...")
            ->execute("su -c 'cd $path && php artisan vendor:publish --force' - nxpanel");
    }
}