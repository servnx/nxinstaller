<?php

namespace NxInstaller\Installer\NxPanel;

use NxInstaller\Installer\BaseInstaller;

class AdminPassword extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();
        $this->config->set('app' . $delim . 'password', str_random(8));
    }
}