<?php

namespace NxInstaller\Installer\NxPanel;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class InitializeNxpanel extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $path = $this->config->get('app' . $delim . 'install_dir');

        (new Process($this->io))
            ->setTitle("Running Initialization ...")
            ->execute("su -c 'cd $path && php artisan nxpanel:init' - nxpanel");
    }
}