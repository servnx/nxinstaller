<?php

namespace NxInstaller\Installer\NxPanel;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class AppKey extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $install_dir = $this->config->get('app' . $delim . 'install_dir');

        if (!file_exists("$install_dir/.env")) {
            throw new \Exception("$install_dir/.env file is not present .. please report this bug!");
        }

        (new Process($this->io))
            ->setTitle("Generating App Key for $install_dir/.env ...")
            ->execute("su -c 'cd $install_dir && php artisan key:generate' - nxpanel");
    }
}