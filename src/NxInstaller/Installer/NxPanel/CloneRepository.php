<?php

namespace NxInstaller\Installer\NxPanel;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class CloneRepository extends BaseInstaller
{
    public function handle()
    {
        // todo: is there any sane way of doing this with a salt state ?

        $delim = $this->config->getDelimiter();

        $install_dir = $this->config->get('app' . $delim . 'install_dir');
        $url = $this->config->get('app' . $delim . 'repository');

        if (is_dir($install_dir)) {
            $this->warning("$install_dir already exists!");

            (new Process($this->io))
                ->setTitle("Removing $install_dir ...")
                ->execute("rm -rf $install_dir");
        }

        $clone_path = dirname($install_dir);

        (new Process($this->io))
            ->setTitle("Cloning Repository ...")
            ->execute("cd $clone_path && git clone $url nxpanel");

        if (!is_dir($install_dir) && !is_file("$install_dir/server.php")) {
            throw new \Exception('Something went wrong with the cloning process. Please report this bug!');
        }
    }
}