<?php

namespace NxInstaller\Installer\Utilities;

use NxInstaller\Installer\BaseInstaller;

class RefreshPillars extends BaseInstaller
{
    public function handle()
    {
        $this->salt->tools()->refreshPillars();
    }
}