<?php

namespace NxInstaller\Installer\Utilities;

use NxInstaller\Installer\BaseInstaller;

class AddPillarsArray extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $pillar_to = $this->config->get('salt' . $delim . 'pillar_path');

        $this->config->addPillars([
            $pillar_to
        ]);
    }
}