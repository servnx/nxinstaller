<?php

namespace NxInstaller\Installer\Utilities;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class RestartMaster extends BaseInstaller
{
    public function handle()
    {
        // todo: need to maybe create a strategy class for determining the OS ?
        //
        // todo: ServiceStrategy (will determine the method to use to control OS Services) ?
        (new Process($this->io))
            ->setTitle("Restarting salt-master ...")
            ->execute("service salt-master restart");
    }
}