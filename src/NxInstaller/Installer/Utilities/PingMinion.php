<?php

namespace NxInstaller\Installer\Utilities;

use NxInstaller\Installer\BaseInstaller;

class PingMinion extends BaseInstaller
{
    public function handle($param = null)
    {
        $target = $param;

        if ($target === null) {
            $target = hostname();
        }

        $finished = false;
        $current = 0;
        $timeout = 5;

        do {
            $current++;
            if ($this->salt->ping($target) === true || $current >= $timeout) {
                $finished = true;
            };
        } while ($finished == false);

        if ($current >= $timeout) {
            $this->warning("Minion did not respond in a timely manner ...");
        };
    }
}