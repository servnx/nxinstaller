<?php

namespace NxInstaller\Installer\Utilities;

use NxInstaller\Installer\BaseInstaller;

class RestartService extends BaseInstaller
{
    public function handle($target = null, $service = null)
    {
        if ($target === null || $service === null) {
            throw new \Exception(
                "Null Argument supplied in handle method for RestartService::class. 
                
                Expecting string value"
            );
        }

        $this->salt->execute($target, 'service.restart', [$service]);
    }
}