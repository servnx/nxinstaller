<?php

namespace NxInstaller\Installer\Utilities;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class RestartMinion extends BaseInstaller
{
    public function handle()
    {
        $pids_before = explode("\n", trim(shell_exec('pgrep salt-minion')));

        // todo: need to maybe create a strategy class for determining the OS ?
        // todo: ServiceStrategy (will determine the method to use to control OS Services) ?
        (new Process($this->io))
            ->setTitle("Restarting salt-minion ...")
            ->execute("service salt-minion restart");

        $pids_after = explode("\n", trim(shell_exec('pgrep salt-minion')));

        if (($pids_before == $pids_after)) {
            throw new \Exception('salt-minion services failed to restart!');
        }
    }
}