<?php

namespace NxInstaller\Installer\Server;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class InstallSalt extends BaseInstaller
{
    public function handle()
    {
        if (!installed('salt')) {
            (new Process($this->io))
                ->setTitle("Installing Salt ...")
                ->execute('sh install_salt.sh -P -M');
        } else {
            $this->warning('Salt is already installed ...');
        }
    }
}