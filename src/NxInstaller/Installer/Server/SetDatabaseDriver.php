<?php

namespace NxInstaller\Installer\Server;

use NxInstaller\Installer\BaseInstaller;

class SetDatabaseDriver extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $driver_choices = array_keys($this->config->get('app' . $delim . 'database_installers'));
        $default_driver = $this->config->get('app' . $delim . 'database_driver');

        $driver = $this->io->choice('Choose a database driver', $driver_choices, $default_driver);
        $this->config->set('app' . $delim . 'database_driver', $driver);
    }
}