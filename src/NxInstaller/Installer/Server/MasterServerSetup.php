<?php

namespace NxInstaller\Installer\Server;

use NxInstaller\Installer\BaseInstaller;
use NxInstaller\Installer\Utilities\AddPillarsArray;
use NxInstaller\Installer\Utilities\PingMinion;
use NxInstaller\Installer\Utilities\RefreshPillars;
use NxInstaller\Installer\Utilities\RestartMinion;

class MasterServerSetup extends BaseInstaller
{
    private $steps = [
        // User Input
        ServerName::class,
        HostName::class,
        ServerIP::class,
        SetDatabaseDriver::class,
        SetDatabasePassword::class,

        // Install
        InstallSalt::class,
        AddMasterIP::class,
        RestartMinion::class,
        AcceptMinionKey::class,
        PingMinion::class,
        CopySaltResources::class,
        CopyPillarResources::class,
        AddPillarsArray::class,
        RefreshPillars::class,
        UsersState::class,
    ];

    public function handle()
    {
        foreach ($this->steps as $class) {
            (new $class($this->io, $this->salt))->handle();
        }
    }
}