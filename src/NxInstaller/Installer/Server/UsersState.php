<?php

namespace NxInstaller\Installer\Server;

use NxInstaller\Installer\BaseInstaller;

class UsersState extends BaseInstaller
{
    public function handle($param = null)
    {
        $target = $param;

        if ($target === null) {
            $target = hostname();
        }

        $delim = $this->config->getDelimiter();

        $this->config->set('app' . $delim . 'nxpanel-shell_password', str_random(16));

        $pass = $this->config->get('app' . $delim . 'nxpanel-shell_password');

        $hash = trim(str_replace("\n", '', shell_exec("openssl passwd -1 -salt xyz $pass")));

        $this->config->setPillar(
            'users' . $delim . 'nxpanel',
            'password',
            $hash
        );

        $data = $this->config->getPillar('users');

        $this->salt->execute($target, 'state.sls', ['users'], $data);
    }
}