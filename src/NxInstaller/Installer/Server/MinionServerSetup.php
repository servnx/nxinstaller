<?php

namespace NxInstaller\Installer\Server;

use NxInstaller\Installer\BaseInstaller;
use NxInstaller\Installer\Utilities\PingMinion;
use NxInstaller\Installer\Utilities\RestartMinion;

class MinionServerSetup extends BaseInstaller
{
    private $steps = [
        AddMasterIP::class,
        RestartMinion::class,
        PingMinion::class
    ];

    public function handle()
    {
        foreach ($this->steps as $class) {
            (new $class($this->io, $this->salt))->handle();
        }
    }
}