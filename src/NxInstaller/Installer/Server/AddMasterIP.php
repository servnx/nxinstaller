<?php

namespace NxInstaller\Installer\Server;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class AddMasterIP extends BaseInstaller
{
    public function handle($param = '127.0.0.1')
    {
        if (!str_contains(file_get_contents('/etc/salt/minion'), "master: $param")) {
            (new Process($this->io))
                ->setTitle("Adding Master IP to /etc/salt/minion ...")
                ->execute("echo 'master: $param' >> /etc/salt/minion");
        } else {
            $this->warning("/etc/salt/minion already contains 'master: $param'");
        }
    }
}