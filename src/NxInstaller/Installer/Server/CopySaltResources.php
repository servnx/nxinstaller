<?php

namespace NxInstaller\Installer\Server;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class CopySaltResources extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $salt_from = $this->config->get('app' . $delim . 'salt-resource_path');

        $salt_to = $this->config->get('salt' . $delim . 'salt_path');

        if (is_dir($salt_to)) {
            $this->warning("$salt_to already exists.");

            if ($this->io->confirm("Would you like to overwrite this directory?", false)) {
                (new Process($this->io))
                    ->setTitle("Removing $salt_to ...")
                    ->execute("rm -rf $salt_to");

                (new Process($this->io))
                    ->setTitle("Copying $salt_to ...")
                    ->execute("cp -R $salt_from $salt_to");
            } else {
                (new Process($this->io))
                    ->setTitle("Syncing $salt_to ...")
                    ->execute("cp -Rn $salt_from $salt_to");
            }
        } else {
            (new Process($this->io))
                ->setTitle("Copying $salt_from to $salt_to ...")
                ->execute("cp -R $salt_from $salt_to");
        }
    }
}