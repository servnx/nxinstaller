<?php

namespace NxInstaller\Installer\Server;

use NxInstaller\Installer\BaseInstaller;

class AcceptMinionKey extends BaseInstaller
{

    public function handle($param = null)
    {
        $target = $param;

        if ($target === null) {
            $target = hostname();
        }

        $this->io->writeln("<fg=magenta>Searching for keys ... Please wait</>");

        if (!in_array($target, $this->salt->getKeys('minions'))) {
            $curr = 0;
            $finished = false;

            do {
                sleep(1);
                $curr++;
                if ($curr >= $this->timeout) {
                    throw new \Exception("Timed out trying to accept the key!");
                }

                if ($this->salt->acceptMinionKey($target)) {
                    $this->io->writeln("<fg=green>'$target' has been Accepted!</>\n");
                    $finished = true;
                }
            } while (!$finished);

        } else {

            $this->warning("'$target' has already been accepted!");

        }
    }
}