<?php

namespace NxInstaller\Installer\Server;

use NxInstaller\Installer\BaseInstaller;

class HostName extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $check = false;
        do {
            $default_name = fqdn();

            $hostname = $this->io->ask('The FQDN for this server', $default_name);

            $check = ($hostname !== '') ? true : false;

            if (!$check) {
                $this->warning('Cannot be empty!');
            }

        } while (!$check);

        $this->config->set('app' . $delim . 'hostname', trim($hostname));
    }
}