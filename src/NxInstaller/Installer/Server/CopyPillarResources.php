<?php

namespace NxInstaller\Installer\Server;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class CopyPillarResources extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $pillar_from = $this->config->get('app' . $delim . 'pillar-resource_path');

        $pillar_to = $this->config->get('salt' . $delim . 'pillar_path');

        if (is_dir($pillar_to)) {
            $this->warning("$pillar_to already exists.");

            if ($this->io->confirm("Would you like to overwrite this directory?", false)) {
                (new Process($this->io))
                    ->setTitle("Removing $pillar_to ...")
                    ->execute("rm -rf $pillar_to");

                (new Process($this->io))
                    ->setTitle("Copying $pillar_to ...")
                    ->execute("cp -R $pillar_from $pillar_to");
            } else {
                (new Process($this->io))
                    ->setTitle("Syncing $pillar_to ...")
                    ->execute("cp -Rn $pillar_from $pillar_to");
            }
        } else {
            (new Process($this->io))
                ->setTitle("Copying $pillar_from to $pillar_to ...")
                ->execute("cp -R $pillar_from $pillar_to");
        }
    }
}