<?php

namespace NxInstaller\Installer\Server;

use NxInstaller\Installer\BaseInstaller;

class SetDatabasePassword extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $password = str_random(16);
        $this->config->set('app' . $delim . 'database_password', $password);
    }
}