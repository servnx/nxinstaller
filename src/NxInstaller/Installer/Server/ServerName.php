<?php

namespace NxInstaller\Installer\Server;

use NxInstaller\Installer\BaseInstaller;

class ServerName extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $check = false;
        do {
            $default_name = hostname();

            $server_name = $this->io->ask('Server Name', $default_name);

            $check = ($server_name !== '') ? true : false;

            if (!$check) {
                $this->warning('Cannot be empty!');
            }
        } while (!$check);

        $this->config->set('app' . $delim . 'server_name', trim($server_name));
    }
}