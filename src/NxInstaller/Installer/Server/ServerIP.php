<?php

namespace NxInstaller\Installer\Server;

use NxInstaller\Installer\BaseInstaller;

class ServerIP extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $check = false;
        do {
            $default_ip = ip();

            $ip = $this->io->ask('Primary IP Address', $default_ip);
            $this->config->set('app' . $delim . 'ip', trim($ip));

            $check = ($this->config->get('app' . $delim . 'ip') !== '') ? true : false;

            if (!$check) {
                $this->warning('Cannot be empty!');
            }
        } while (!$check);
    }
}