<?php

namespace NxInstaller\Installer\Database;

use NxInstaller\Installer\BaseInstaller;

class DriverInstaller extends BaseInstaller
{
    public function handle()
    {
        $delim = $this->config->getDelimiter();

        $driver = $this->config->get('app' . $delim . 'database_driver');
        $installer = $this->config->get('app' . $delim . 'database_installers')[$driver];

        (new $installer($this->io, $this->salt))->handle();
    }
}