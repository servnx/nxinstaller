<?php

namespace NxInstaller\Installer\Database\MySql;

use NxInstaller\Installer\BaseInstaller;
use NxInstaller\Installer\Utilities\PingMinion;
use NxInstaller\Installer\Utilities\RestartMinion;

class MySqlInstaller extends BaseInstaller
{
    private $steps = [
        MySqlServerState::class,
        MySqlMinionConfig::class,
        RestartMinion::class,
        PingMinion::class,
        MySqlDatabaseState::class,
        MySqlUserState::class
    ];

    public function handle()
    {
        foreach ($this->steps as $class) {
            (new $class($this->io, $this->salt))->handle();
        }

    }
}