<?php

namespace NxInstaller\Installer\Database\MySql;

use NxInstaller\Installer\BaseInstaller;

class MySqlServerState extends BaseInstaller
{
    public function handle($param = null)
    {
        $target = $param;

        if ($target === null) {
            $target = hostname();
        }

        $this->salt->execute($target, 'state.sls', ['mysql.server']);
    }
}