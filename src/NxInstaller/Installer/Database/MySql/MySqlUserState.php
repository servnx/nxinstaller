<?php

namespace NxInstaller\Installer\Database\MySql;

use NxInstaller\Installer\BaseInstaller;

class MySqlUserState extends BaseInstaller
{
    public function handle($param = null)
    {
        $target = $param;

        if ($target === null) {
            $target = hostname();
        }

        $delim = $this->config->getDelimiter();

        $password = $this->config->get('app' . $delim . 'database_password');

        $this->config->setPillar(
            'mysql' . $delim . 'user' . $delim . 'dbnxpanel',
            'password',
            $password
        );

        $data = $this->config->getPillar('mysql');

        $this->salt->execute($target, 'state.sls', ['mysql.user'], $data);
    }
}