<?php

namespace NxInstaller\Installer\Database\MySql;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\BaseInstaller;

class MySqlMinionConfig extends BaseInstaller
{
    public function handle()
    {
        $unix_socket = trim(shell_exec('mysql -sNe "SHOW VARIABLES LIKE \'socket\'" | sed -e "s|socket\s||"'));

        $contents = file_get_contents('/etc/salt/minion');

        if (!str_contains($contents, "mysql.charset: 'utf8'")) {
            (new Process($this->io))
                ->setTitle("Adding mysql.charset to Minion Config ...")
                ->execute('echo "mysql.charset: \'utf8\'" >> /etc/salt/minion');
        } else {
            $this->warning("mysql.charset: 'utf8' has already been added to /etc/salt/minion");
        }

        if (!str_contains($contents, "mysql.unix_socket: '$unix_socket'")) {
            (new Process($this->io))
                ->setTitle("Adding mysql.unix_socket to Minion Config ...")
                ->execute('echo "mysql.unix_socket: \'' . $unix_socket . '\'" >> /etc/salt/minion');
        } else {
            $this->warning("mysql.unix_socket: '$unix_socket' has already been added to /etc/salt/minion");
        }
    }
}