<?php

namespace NxInstaller\Installer;

use NxInstaller\Classes\Process;
use NxInstaller\Installer\Server\MasterServerSetup;
use NxInstaller\Installer\Database\DriverInstaller;
use NxInstaller\Installer\NxPanel\NxPanelInstaller;
use NxInstaller\Installer\Policies\InstallerPolicies;
use NxInstaller\Installer\Prerequisites\MasterPrerequisites;
use NxInstaller\Installer\Nginx\NginxInstaller;
use NxInstaller\Installer\Utilities\RestartService;

class MasterInstaller extends BaseInstaller
{
    private $steps = [
        InstallerPolicies::class => "Validating Installer Policies",
        MasterServerSetup::class => 'Server Setup',
        MasterPrerequisites::class => 'Prerequisites',
        NginxInstaller::class => 'Nginx Web Server',
        DriverInstaller::class => 'Database Setup',
        NxPanelInstaller::class => 'NxPanel Interface Setup',
    ];

    public function handle()
    {
        foreach ($this->steps as $class => $header) {
            $this->head($header);

            (new $class($this->io, $this->salt))->handle();

            $this->done();
        }

        $this->head("Finalizing Install");

        $this->cleanup();

        $this->restartServices();

        $this->info();

        $this->done();
    }

    private function cleanup()
    {
        $delim = $this->config->getDelimiter();

        $path = $this->config->get('app' . $delim . 'install_dir');

        if (file_exists("$path/init.json")) {
            (new Process($this->io))
                ->setTitle("Removing $path/init.json file ....")
                ->execute("rm $path/init.json");
        }
    }

    private function restartServices()
    {
        $services = $this->salt->tools()->getServices(hostname(), 'php');
        array_push($services, 'nginx');

        foreach ($services as $service) {
            (new RestartService($this->io, $this->salt))->handle(hostname(), $service);
        }
    }

    public function info()
    {
        $delim = $this->config->getDelimiter();

        $dbpass = $this->config->get('app' . $delim . 'database_password');

        $nxpanel_shell_password = $this->config->get('app' . $delim . 'nxpanel-shell_password');

        $this->io->note([
            ' ',
            'Nxpanel Shell Login: nxpanel',
            'Nxpanel Shell Password: ' . $nxpanel_shell_password,
            ' ',
            'Database: dbnxpanel',
            'Database User: dbnxpanel',
            'Database Password: ' . $dbpass
        ]);

        $domain = $this->config->get('app' . $delim . 'hostname');
        $pass = $this->config->get('app' . $delim . 'password');

        $this->io->writeln([
            '<fg=green>NxPanel has been successfully installed!',
            ' ',
            "Visit $domain:8000 in a browser",
            ' ',
            "Login: admin@$domain",
            "Password: $pass",
            '</>'
        ]);
    }

}