<?php

namespace NxInstaller\Installer;

class SingleInstaller extends BaseInstaller
{
    private $steps = [

    ];

    public function handle()
    {
        // for now (until there is a reason not to) we will defer to the MasterInstaller
        (new MasterInstaller($this->io, $this->salt))->handle();

        /*foreach ($this->steps as $class => $header) {
            (new $class($this->io, $this->salt))
                ->setHeader($header)
                ->handle();
        }*/
    }

}