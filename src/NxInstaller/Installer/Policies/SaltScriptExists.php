<?php

namespace NxInstaller\Installer\Policies;

class SaltScriptExists extends BasePolicy
{
    public function allows($param = null)
    {
        $delim = $this->config->getDelimiter();

        if (!file_exists($this->config->get('app' . $delim . 'salt-install_script'))) {
            throw new \Exception(
                "The file specified for 'salt-install_script' in 'app' config was not found!
                
                Please fix this and try again!
                
                Example: 'salt-install_script' => __DIR__ . '/../../../install_salt.sh',"
            );
        }
    }
}