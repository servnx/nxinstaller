<?php

namespace NxInstaller\Installer\Policies;

class DatabaseInstallersKeyExists extends BasePolicy
{
    public function allows($param = null)
    {
        $delim = $this->config->getDelimiter();

        if (!array_key_exists('database_installers', $this->config->get('app'))) {
            throw new \Exception(
                "The key 'database_installers' was not found in 'app' config!
                
                Please fix this and try again.
                 
                Example: 'database_installers' => [
                              'MySql' => MySqlInstaller::class,
                         ],"
            );
        }

        $database_installers = $this->config->get('app' . $delim . 'database_installers');

        if (!is_array($database_installers)) {
            throw new \Exception(
                "The key 'database_installers' is NOT an array in 'app' config!
                
                'database_installers' should be an array of database drivers
                mapped with their respected installer class.
                 
                Example: 'database_installers' => [
                              'MySql' => MySqlInstaller::class,
                         ],"
            );
        }

        if (count($database_installers) <= 0) {
            throw new \Exception(
                "The key 'database_installers' is an empty array in 'app' config!
                
                'database_installers' should be an array of at least 1 database driver
                mapped with its respected installer class.
                 
                Example: 'database_installers' => [
                              'MySql' => MySqlInstaller::class,
                         ],"
            );
        }
    }
}