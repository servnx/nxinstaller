<?php

namespace NxInstaller\Installer\Policies;

use Salt\Salt;
use Salt\SaltConfig;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class BasePolicy
{
    /**
     * @var SymfonyStyle
     */
    protected $io;

    /**
     * @var Salt
     */
    protected $salt;

    /**
     * @var SaltConfig
     */
    protected $config;

    public function __construct(SymfonyStyle $io, Salt $salt)
    {
        $this->io = $io;

        $this->salt = $salt;
        $this->config = $salt->config();
    }

    public function handle($param = null)
    {
        $this->allows($param);
    }

    abstract public function allows($param = null);
}