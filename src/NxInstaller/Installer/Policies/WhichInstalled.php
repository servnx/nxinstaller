<?php

namespace NxInstaller\Installer\Policies;

class WhichInstalled extends BasePolicy
{
    public function allows($param = null)
    {
        if (installed('which')) {
            throw new \Exception("'which' must be installed on your system!");
        }
    }
}