<?php

namespace NxInstaller\Installer\Policies;

class AppNotInstalled extends BasePolicy
{
    public function allows($param = null)
    {
        $delim = $this->config->getDelimiter();

        if (is_dir($this->config->get('app' . $delim . 'install_dir'))) {
            throw new \Exception('NxPanel already installed ... Exiting installer');
        }
    }
}