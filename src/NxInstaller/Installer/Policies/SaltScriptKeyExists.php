<?php

namespace NxInstaller\Installer\Policies;

class SaltScriptKeyExists extends BasePolicy
{
    public function allows($param = null)
    {
        $delim = $this->config->getDelimiter();

        if (!array_key_exists('salt-install_script', $this->config->get('app'))) {
            throw new \Exception(
                "The key 'salt-install_script' was not found in 'app' config!
                
                Please fix this and try again!
                
                Example: 'salt-install_script' => __DIR__ . '/../../../install_salt.sh',"
            );
        }
    }
}