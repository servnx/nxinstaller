<?php

namespace NxInstaller\Installer\Policies;

class StubDirKeyExists extends BasePolicy
{
    public function allows($param = null)
    {
        if (!array_key_exists('stub_dir', $this->config->get('app'))) {
            throw new \Exception(
                "The key 'stub_dir' was not found in 'app' config!
                
                Please fix this and try again.
                 
                Example: 'stub_dir' => __DIR__ . '/../Resources/stubs',"
            );
        }
    }
}