<?php

namespace NxInstaller\Installer\Policies;

class FpmPoolPathKeyExists extends BasePolicy
{
    public function allows($param = null)
    {
        $delim = $this->config->getDelimiter();

        $os = strtolower($this->salt->os(hostname()));

        if (!array_key_exists('php-fpm-pool_path', $this->config->get('os' . $delim . $os))) {
            throw new \Exception(
                "The key 'php-fpm-pool_path' was not found in os.php '$os' config!
                
                Please fix this and try again.
                 
                Example: 'php-fpm-pool_path' => '/etc/php/7.0/fpm/pool.d',"
            );
        }
    }
}