<?php

namespace NxInstaller\Installer\Policies;

class InstallerPolicies extends BasePolicy
{
    private $steps = [
        /*
         * Configuration policies
         */
        AppKeyExists::class,
        InstallDirKeyExists::class,
        StubDirKeyExists::class,
        SaltResourcePathKeyExists::class,
        PillarResourcePathKeyExists::class,
        //FpmPoolPathKeyExists::class, todo: these need to be updated to reflect the os.php config
        //FpmSockPathKeyExists::class,
        PhpVersionKeyExists::class,
        SaltScriptKeyExists::class,
        DatabaseInstallersKeyExists::class,
        DatabaseDriverKeyExists::class,

        /*
         * Other policies
         */
        IsPrivilegedUser::class,
        //AppNotInstalled::class,
        IspcNotInstalled::class,
        IsPhpVersionCompat::class,
        SaltScriptExists::class
    ];

    public function allows($param = null)
    {
        foreach ($this->steps as $class) {
            (new $class($this->io, $this->salt))->allows($param);
        }
    }
}