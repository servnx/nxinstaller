<?php

namespace NxInstaller\Installer\Policies;

class DatabaseDriverKeyExists extends BasePolicy
{
    public function allows($param = null)
    {
        $delim = $this->config->getDelimiter();

        if (!array_key_exists('database_driver', $this->config->get('app'))) {
            throw new \Exception("The key 'database_driver' was not found in 'app' config!
                
                Please fix this and try again.
                 
                Example: 'database_driver' => 'MySql',"
            );
        }

        $driver = $this->config->get('app' . $delim . 'database_driver');

        if (!is_string($driver)) {
            throw new \Exception("The key 'database_driver' is NOT a string in 'app' config!
                
                'database_driver' should be a string value of the Default database driver.
                 
                Example: 'database_driver' => 'MySql',"
            );
        }

        if (!array_key_exists($driver, $this->config->get('app' . $delim . 'database_installers'))) {
            throw new \Exception("The key 'database_driver' is invalid in 'app' config!
                
                'database_driver' should match a key listed in 'database_installers' array.
                 
                Example: 'database_driver' => 'MySql',
                 
                         'database_installers' => [
                              'MySql' => MySqlInstaller::class,
                         ],"
            );
        }
    }
}