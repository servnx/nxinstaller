<?php

namespace NxInstaller\Installer\Policies;

class AppKeyExists extends BasePolicy
{
    public function allows($param = null)
    {
        if (!array_key_exists('app', $this->config->getValues())) {
            throw new \Exception(
                "The key 'app' was not found in config!
                
                Please be sure to initialize SaltConfig with a directory specified.
                 
                Example: new SaltConfig(__DIR__ . '/../Configs');
                
                Inside the 'Configs' directory there should be a file named 'app.php'
                with the application configuration."
            );
        }
    }
}