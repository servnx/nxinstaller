<?php

namespace NxInstaller\Installer\Policies;

class FpmSockPathKeyExists extends BasePolicy
{
    public function allows($param = null)
    {
        if (!array_key_exists('php-fpm-sock_path', $this->config->get('app'))) {
            throw new \Exception(
                "The key 'php-fpm-sock_path' was not found in 'app' config!
                
                Please fix this and try again.
                 
                Example: 'php-fpm-sock_path' => '/var/run/php',"
            );
        }
    }
}