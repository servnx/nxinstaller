<?php

namespace NxInstaller\Installer\Policies;

class PillarResourcePathKeyExists extends BasePolicy
{
    public function allows($param = null)
    {
        if (!array_key_exists('salt-resource_path', $this->config->get('app'))) {
            throw new \Exception(
                "The key 'salt-resource_path' was not found in 'app' config!
                
                Please add 'salt-resource_path' with a value containing a directory path.
                 
                Example: 'salt-resource_path' => __DIR__ . '/../Resources/salt',"
            );
        }
    }
}