<?php

namespace NxInstaller\Installer\Policies;

class IspcNotInstalled extends BasePolicy
{
    public function allows($param = null)
    {
        if (is_dir('/root/ispconfig') || is_dir('/home/admispconfig') || is_dir('/usr/local/ispconfig')) {
            throw new \Exception('NxPanel cannot be installed with ISPConfig');
        }
    }
}