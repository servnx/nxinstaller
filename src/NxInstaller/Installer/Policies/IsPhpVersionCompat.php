<?php

namespace NxInstaller\Installer\Policies;

class IsPhpVersionCompat extends BasePolicy
{
    public function allows($param = null)
    {
        $delim = $this->config->getDelimiter();

        $phpv = $this->config->get('app' . $delim . 'min_php_version');
        if (version_compare(PHP_VERSION, $phpv, '<')) {
            throw new \Exception('PHP Version is not compatible with NxPanel. Install PHP ' . $phpv . ' or later...');
        }
    }
}