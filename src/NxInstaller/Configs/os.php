<?php

return [
    // todo: need to create new policies

    // todo: feature to be added, add ability to set these paths using an expert mode ?
    // todo: and/or even add  installer options like --php-fpm-pool-dir="/etc/php/7.0/fpm/pool.d"

    // keys are supported OS's

    'debian' => [
        'php-fpm-pool_path' => '/etc/php/' . PHP_MAJOR_VERSION . '.' . PHP_MINOR_VERSION . '/fpm/pool.d',
        'php-fpm-sock_path' => '/var/run/php',
        'php-fpm-listen_user' => 'www-data',
        'php-fpm-listen_group' => 'www-data'
    ],

    'ubuntu' => [
        'php-fpm-pool_path' => '/etc/php/' . PHP_MAJOR_VERSION . '.' . PHP_MINOR_VERSION . '/fpm/pool.d',
        'php-fpm-sock_path' => '/var/run/php',
        'php-fpm-listen_user' => 'www-data',
        'php-fpm-listen_group' => 'www-data'
    ],

    'centos' => [
        'php-fpm-pool_path' => '/etc/php-fpm.d',
        'php-fpm-sock_path' => '/var/run/php-fpm',
        'php-fpm-listen_user' => 'nginx',
        'php-fpm-listen_group' => 'nginx'
    ],
];