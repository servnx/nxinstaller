<?php

use NxInstaller\Installer\Database\MySql\MySqlInstaller;

return [
    // todo: create policies

    'install_dir' => '/var/www/nxpanel',

    'stub_dir' => __DIR__ . '/../Resources/stubs',

    'salt-resource_path' => __DIR__ . '/../Resources/salt',

    'pillar-resource_path' => __DIR__ . '/../Resources/pillar',

    'salt-install_script' => __DIR__ . '/../../../install_salt.sh',

    'min_php_version' => '5.6',

    // todo: repository should be moved to an appropriate GitHub (eventually)
    'repository' => 'https://poppabear@bitbucket.org/servnx/nxpanel.git',

    'database_driver' => 'MySql',

    'database_installers' => [
        /**
         * Map to the Installer class
         */
        'MySql' => MySqlInstaller::class,

        //'MariaDB' => '', todo: need to create a state for MariaDB (No formula is available)
        //'Postgres' => '', todo: need to add Postgres formula
    ],

    /*
     * Dynamically set configuration defaults and place holders
     *
     * These do NOT need policy classes
     */
    'nxpanel-shell_password' => '',
    'server_name' => 'webserver1',
    'ip' => '127.0.0.1',
    'hostname' => 'webserver1.my_fqdn_domain.tld',
    'password' => '',

    'database_password' => '',

];