<?php

namespace NxInstaller\Commands;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use NxInstaller\Traits\ConsoleTools;

class UninstallCommand extends ContainerAwareCommand
{
    use ConsoleTools;

    private $input;
    private $output;
    private $io;

    protected function configure()
    {
        $this
            ->setName('nxpanel:uninstall')
            ->setDescription('Uninstalls NxPanel')
            ->setHelp("This command uninstalls NxPanel and its components ...");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        // lets initialize some class properties
        $this->input = $input;
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $output);

        // display the intro header
        $this->displayIntro();

        $who = trim(shell_exec('whoami'));
        if ($who !== 'root') {
            $this->throwError('Must ba ran as root or using sudo!');
        }

        $salt_installed = trim(shell_exec('which salt'));
        if ($salt_installed == '') {
            $this->throwError('Salt is NOT installed, likely because nxpanel is already uninstalled.');
        }

        // lets make sure the user understands how to use this installer
        $this->io->writeln([
            'Following will uninstall NxPanel and \'some\' packages that were installed with it.',
            'While we try to be sure the uninstall is fully tested and fully functional,',
            'We can NOT be held responsible for ANY damage caused by it.'
        ]);

        if (!$this->io->confirm('I Agree and wish to continue', false)) {
            $this->throwError('Canceled ...');
        }

        $hostname = hostname();

        $this->head('Uninstaller');

        // Let salt uninstall most of everything using states.
        $this->process("salt '$hostname' state.sls uninstaller", 'Running uninstaller state ...', true);

        // todo: is this the same across OS's ? Likely not ...
        $this->process('pkill -f "salt-"');

        // todo: how should we handle these few processes, abviously they would be OS specific also
        $this->process("apt-get purge -y --auto-remove salt-master", "Removing 'salt-master' ...");
        $this->process("apt-get purge -y --auto-remove salt-minion", "Removing 'salt-minion' ...");

        $this->process("rm -rf /srv/salt && rm -rf /srv/pillar && rm -rf /etc/salt", "Removing salt resources ...");

        $this->success();
    }
}