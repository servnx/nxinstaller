<?php

namespace NxInstaller\Commands;

use NxInstaller\Classes\SaltCli;
use NxInstaller\Installer\SingleInstaller;
use Salt\SaltConfig;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use NxInstaller\Installer\MasterInstaller;
use NxInstaller\Traits\ConsoleTools;
use Symfony\Component\Intl\Exception\NotImplementedException;


class InstallCommand extends ContainerAwareCommand
{
    use ConsoleTools;

    /**
     * SymfonyStyle Instance
     *
     * @var $io
     */
    private $io;

    /**
     * SaltCli Instance
     *
     * @var $salt
     */
    private $salt;

    /**
     * Config Instance
     *
     * @var $config
     */
    private $config;

    protected function configure()
    {
        $this
            ->setName('nxpanel:install')
            ->setDescription('Installs NxPanel')
            ->setHelp("This command installs NxPanel...")
            ->setDefinition(
                new InputDefinition([
                    new InputOption('single', 'S', null, 'Installs as a single server setup!'),
                    new InputOption('master', 'M', null, 'Installs as the master server!'),
                    new InputOption('minion', 'N', null, 'Installs as a minion (slave) server!'),
                ])
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->config = new SaltConfig(__DIR__ . '/../Configs');
        $this->salt = new SaltCli($this->io, $this->config);

        // display the intro header
        $this->displayIntro();

        // lets make sure the user understands how to use this installer
        $this->seperator();
        $this->io->writeln([
            'Following will be a few questions for primary configuration so be careful.',
            'Default values are in [brackets] and can be accepted with <ENTER>.',
            'CTRL + C anytime to stop the installer.'
        ]);
        $this->seperator();

        if (!$this->io->confirm('Continue ?', false)) {
            $this->throwError('Canceled ...');
        }

        if ($input->getOption('single')) {

            (new SingleInstaller($this->io, $this->salt))->handle();

        } elseif ($input->getOption('master')) {

            (new MasterInstaller($this->io, $this->salt))->handle();

        } elseif ($input->getOption('minion')) {

            // todo: create MinionInstaller
            //$this->minion();

        } else {

            // No options where passed so lets ask manually
            $this->head('Please choose a option below that describes what you want to do.');

            $this->io->table(['Option', 'Description'], [
                ['single', 'Installs as a single server setup! Use if this will be the ONLY server!'],
                ['master', 'Installs as the master server! Use if this server is the 1st of multiple servers.'],
                [
                    'minion',
                    'Installs as a minion (slave) server! Use if this server will be added to an existing master.'
                ]
            ]);

            $type = $this->io->choice('Install Type', ['Single', 'Master', 'Minion']);

            switch ($type) {
                case 'Single':
                    (new SingleInstaller($this->io, $this->salt))->handle();
                    break;
                case 'Master':
                    (new MasterInstaller($this->io, $this->salt))->handle();
                    break;
                case 'Minion':
                    throw new NotImplementedException("Minion installer has not yet been implemented!");
                    break;
                default:
                    (new SingleInstaller($this->io, $this->salt))->handle();
            }
        }
    }

    /**
     * Installs a minion server configuration
     */
    private function minion()
    {
        $this->head('Installing Minion ...');

        $this->process('sh install_salt.sh -P', "Installing Salt ...");

        $host = $this->io->ask('Masters IP');
        // todo: need to validate IP

        $this->process("echo 'master: $host' >> /etc/salt/minion");
        $this->process('service salt-minion restart');

        $this->io->note([
            'Your minion has been configured and is ready to receive instructions from the master server.',
            'You must accept the minion connection from the interface panel on your master server.',
            'Once accepted, you can provision your minion from the interface panel!'
        ]);

        $this->success();
    }
}