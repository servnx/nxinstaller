<?php

namespace NxInstaller\Traits;

use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

trait ConsoleTools
{

    /**
     * Formats a warning output
     *
     * @param string $msg
     */
    protected function warning($msg)
    {
        $this->io->writeln("<bg=white;fg=yellow>[Warning] $msg</>\n");
    }

    /**
     * Returns the installer error and exits the installer
     *
     * @param array|string $error
     */
    protected function throwError($error = 'Unknown Error ...')
    {
        $this->io->writeln("<fg=red>$error</>");
        exit(1);
    }

    protected function dump($var)
    {
        $this->io->writeln('<fg=red>---VAR DUMP---');
        var_dump($var);
        $this->io->writeln('--------------</>');
    }

    /**
     * Displays a the intro
     */
    protected function displayIntro()
    {
        $stub = file_get_contents(__DIR__ . '/../Resources/stubs/intro.stub');

        $this->io->text($stub);
    }

    /**
     * Writes a seperator
     */
    protected function seperator()
    {
        $this->io->writeln(str_repeat('=', 80));
    }

    /**
     * Formats a header
     *
     * @param string $text
     */
    protected function head($text)
    {
        $this->io->writeln("\n<fg=black;bg=white>" . str_repeat('=', 80));
        $this->io->writeln('    ' . $text . str_repeat(' ', (76 - strlen($text))));
        $this->io->writeln(str_repeat('=', 80) . "</>\n");
    }

    protected function success()
    {
        $this->io->writeln("\n<fg=black;bg=green>[OK] Done!" . str_repeat(' ', 70) . "</>");
    }
}