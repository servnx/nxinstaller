<?php

use Symfony\Component\VarDumper\VarDumper;
use Symfony\Component\Yaml\Yaml;

if (!function_exists('installed')) {
    function installed($name)
    {
        if (shell_exec("which $name") != '') {
            return true;
        }

        return false;
    }
}

if (!function_exists('base_path')) {
    function base_path($path = '')
    {
        return __DIR__ . "/../../$path";
    }
}

if (!function_exists('resource_path')) {
    function resource_path($path = '')
    {
        return __DIR__ . "/../Resources/$path";
    }
}

if (!function_exists('str_contains')) {
    function str_contains($haystack, $needles)
    {
        foreach ((array)$needles as $needle) {
            if ($needle != '' && mb_strpos($haystack, $needle) !== false) {
                return true;
            }
        }

        return false;
    }
}

if (!function_exists('str_random')) {
    /**
     * Generate a more truly "random" alpha-numeric string.
     *
     * @param  int $length
     * @return string
     */
    function str_random($length = 16)
    {
        $string = '';

        while (($len = strlen($string)) < $length) {
            $size = $length - $len;

            $bytes = random_bytes($size);

            $string .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
        }

        return $string;
    }
}

if (!function_exists('fqdn')) {
    function fqdn()
    {
        return trim(exec('hostname -f'));
    }
}

if (!function_exists('hostname')) {
    function hostname()
    {
        return trim(exec('hostname'));
    }
}

if (!function_exists('ip')) {
    function ip()
    {
        return trim(explode(' ', exec('hostname -I'))[0]);
    }
}

if (!function_exists('pillar2array')) {
    function yaml2array($file)
    {
        if (!file_exists($file)) {
            throw new \Exception($file . ' does NOT exist');
        }

        return Yaml::parse(file_get_contents($file));
    }
}

if (!function_exists('dd')) {
    function dd($var)
    {
        VarDumper::dump($var);
        die();
    }
}

if (!function_exists('array_find')) {
    function array_find($array, $searchKey = '')
    {
        //create a recursive iterator to loop over the array recursively
        $iter = new RecursiveIteratorIterator(
            new RecursiveArrayIterator($array),
            RecursiveIteratorIterator::SELF_FIRST);

        //loop over the iterator
        foreach ($iter as $key => $value) {
            //if the key matches our search
            if ($key === $searchKey) {
                //add the current key
                $keys = array($key);
                //loop up the recursive chain
                for ($i = $iter->getDepth() - 1; $i >= 0; $i--) {
                    //add each parent key
                    array_unshift($keys, $iter->getSubIterator($i)->key());
                }
                //return our output array
                return array('path' => implode('.', $keys), 'value' => $value);
            }
        }

        //return false if not found
        return false;
    }
}

if (!function_exists('distinfo')) {
    function distinfo()
    {
        $distname = '';
        $distver = '';
        $distid = '';
        $distbaseid = '';

        //** Debian or Ubuntu
        if (file_exists('/etc/debian_version')) {

            // Check if this is Ubuntu and not Debian
            if (strstr(trim(file_get_contents('/etc/issue')), 'Ubuntu')
                || (is_file('/etc/os-release') && stristr(file_get_contents('/etc/os-release'), 'Ubuntu'))
            ) {

                $issue = file_get_contents('/etc/issue');

                // Use content of /etc/issue file
                if (strstr($issue, 'Ubuntu')) {
                    if (strstr(trim($issue), 'LTS')) {
                        $lts = " LTS";
                    } else {
                        $lts = "";
                    }
                    $distname = 'Ubuntu';
                    $distid = 'debian40';
                    $distbaseid = 'debian';
                    $ver = explode(' ', $issue);
                    $ver = array_filter($ver);
                    $ver = next($ver);
                    $mainver = explode('.', $ver);
                    $mainver = array_filter($mainver);
                    $mainver = current($mainver) . '.' . next($mainver);
                    // Use content of /etc/os-release file
                } else {
                    $os_release = file_get_contents('/etc/os-release');
                    if (strstr(trim($os_release), 'LTS')) {
                        $lts = " LTS";
                    } else {
                        $lts = "";
                    }

                    $distname = 'Ubuntu';
                    $distid = 'debian40';
                    $distbaseid = 'debian';
                    preg_match("/.*VERSION=\"(.*)\".*/ui", $os_release, $ver);
                    $ver = str_replace("LTS", "", $ver[1]);
                    $ver = explode(" ", $ver, 2);
                    $ver = reset($ver);
                    $mainver = $ver;
                }
                switch ($mainver) {
                    case "16.10":
                        $relname = "(Yakkety Yak)";
                        $distconfid = 'ubuntu1604';
                        break;
                    case "16.04":
                        $relname = "(Xenial Xerus)";
                        $distconfid = 'ubuntu1604';
                        break;
                    case "15.10":
                        $relname = "(Wily Werewolf)";
                        break;
                    case "15.04":
                        $relname = "(Vivid Vervet)";
                        break;
                    case "14.10":
                        $relname = "(Utopic Unicorn)";
                        break;
                    case "14.04":
                        $relname = "(Trusty Tahr)";
                        break;
                    case "13.10":
                        $relname = "(Saucy Salamander)";
                        break;
                    case "13.04":
                        $relname = "(Raring Ringtail)";
                        break;
                    case "12.10":
                        $relname = "(Quantal Quetzal)";
                        break;
                    case "12.04":
                        $relname = "(Precise Pangolin)";
                        break;
                    case "11.10":
                        $relname = "(Oneiric Ocelot)";
                        break;
                    case "11.14":
                        $relname = "(Natty Narwhal)";
                        break;
                    case "10.10":
                        $relname = "(Maverick Meerkat)";
                        break;
                    case "10.04":
                        $relname = "(Lucid Lynx)";
                        break;
                    case "9.10":
                        $relname = "(Karmic Koala)";
                        break;
                    case "9.04":
                        $relname = "(Jaunty Jackpole)";
                        break;
                    case "8.10":
                        $relname = "(Intrepid Ibex)";
                        break;
                    case "8.04":
                        $relname = "(Hardy Heron)";
                        break;
                    case "7.10":
                        $relname = "(Gutsy Gibbon)";
                        break;
                    case "7.04":
                        $relname = "(Feisty Fawn)";
                        break;
                    case "6.10":
                        $relname = "(Edgy Eft)";
                        break;
                    case "6.06":
                        $relname = "(Dapper Drake)";
                        break;
                    case "5.10":
                        $relname = "(Breezy Badger)";
                        break;
                    case "5.04":
                        $relname = "(Hoary Hedgehog)";
                        break;
                    case "4.10":
                        $relname = "(Warty Warthog)";
                        break;
                    default:
                        $relname = "UNKNOWN";
                }
                $distver = $ver . $lts . " " . $relname;
            } elseif (trim(file_get_contents('/etc/debian_version')) == '4.0') {
                $distname = 'Debian';
                $distver = '4.0';
                $distid = 'debian40';
                $distbaseid = 'debian';

            } elseif (strstr(trim(file_get_contents('/etc/debian_version')), '5.0')) {
                $distname = 'Debian';
                $distver = 'Lenny';
                $distid = 'debian40';
                $distbaseid = 'debian';

            } elseif (strstr(trim(file_get_contents('/etc/debian_version')),
                    '6.0') || trim(file_get_contents('/etc/debian_version')) == 'squeeze/sid'
            ) {
                $distname = 'Debian';
                $distver = 'Squeeze/Sid';
                $distid = 'debian60';
                $distbaseid = 'debian';

            } elseif (strstr(trim(file_get_contents('/etc/debian_version')),
                    '7.0') || substr(trim(file_get_contents('/etc/debian_version')), 0,
                    2) == '7.' || trim(file_get_contents('/etc/debian_version')) == 'wheezy/sid'
            ) {
                $distname = 'Debian';
                $distver = 'Wheezy/Sid';
                $distid = 'debian60';
                $distbaseid = 'debian';

            } elseif (strstr(trim(file_get_contents('/etc/debian_version')),
                    '8') || substr(trim(file_get_contents('/etc/debian_version')), 0, 1) == '8'
            ) {
                $distname = 'Debian';
                $distver = 'Jessie';
                $distid = 'debian60';
                $distbaseid = 'debian';

            } elseif (strstr(trim(file_get_contents('/etc/debian_version')), '/sid')) {
                $distname = 'Debian';
                $distver = 'Testing';
                $distid = 'debian60';
                $distconfid = 'debiantesting';
                $distbaseid = 'debian';

            } else {
                $distname = 'Debian';
                $distver = 'Unknown';
                $distid = 'debian40';
                $distbaseid = 'debian';

            }
        } //** Devuan
        elseif (file_exists('/etc/devuan_version')) {
            if (false !== strpos(trim(file_get_contents('/etc/devuan_version')), 'jessie')) {
                $distname = 'Devuan';
                $distver = 'Jessie';
                $distid = 'debian60';
                $distbaseid = 'debian';

            } elseif (false !== strpos(trim(file_get_contents('/etc/devuan_version')), 'ceres')) {
                $distname = 'Devuan';
                $distver = 'Ceres';
                $distid = 'debiantesting';
                $distbaseid = 'debian';

            }
        } //** OpenSuSE
        elseif (file_exists('/etc/SuSE-release')) {
            if (stristr(file_get_contents('/etc/SuSE-release'), '11.0')) {
                $distname = 'openSUSE';
                $distver = '11.0';
                $distid = 'opensuse110';
                $distbaseid = 'opensuse';

            } elseif (stristr(file_get_contents('/etc/SuSE-release'), '11.1')) {
                $distname = 'openSUSE';
                $distver = '11.1';
                $distid = 'opensuse110';
                $distbaseid = 'opensuse';

            } elseif (stristr(file_get_contents('/etc/SuSE-release'), '11.2')) {
                $distname = 'openSUSE';
                $distver = '11.2';
                $distid = 'opensuse112';
                $distbaseid = 'opensuse';

            } else {
                $distname = 'openSUSE';
                $distver = 'Unknown';
                $distid = 'opensuse112';
                $distbaseid = 'opensuse';

            }
        } //** Redhat
        elseif (file_exists('/etc/redhat-release')) {
            $content = file_get_contents('/etc/redhat-release');
            if (stristr($content, 'Fedora release 9 (Sulphur)')) {
                $distname = 'Fedora';
                $distver = '9';
                $distid = 'fedora9';
                $distbaseid = 'fedora';

            } elseif (stristr($content, 'Fedora release 10 (Cambridge)')) {
                $distname = 'Fedora';
                $distver = '10';
                $distid = 'fedora9';
                $distbaseid = 'fedora';

            } elseif (stristr($content, 'Fedora release 10')) {
                $distname = 'Fedora';
                $distver = '11';
                $distid = 'fedora9';
                $distbaseid = 'fedora';

            } elseif (stristr($content, 'CentOS release 5.2 (Final)')) {
                $distname = 'CentOS';
                $distver = '5.2';
                $distid = 'centos52';
                $distbaseid = 'fedora';

            } elseif (stristr($content, 'CentOS release 5.3 (Final)')) {
                $distname = 'CentOS';
                $distver = '5.3';
                $distid = 'centos53';
                $distbaseid = 'fedora';

            } elseif (stristr($content, 'CentOS release 5')) {
                $distname = 'CentOS';
                $distver = 'Unknown';
                $distid = 'centos53';
                $distbaseid = 'fedora';

            } elseif (stristr($content, 'CentOS Linux release 6')) {
                $distname = 'CentOS';
                $distver = 'Unknown';
                $distid = 'centos53';
                $distbaseid = 'fedora';

            } elseif (stristr($content, 'CentOS Linux release 7')) {
                $distname = 'CentOS';
                $distver = 'Unknown';
                $distbaseid = 'fedora';

                $var = explode(" ", $content);
                $var = explode(".", $var[3]);
                $var = $var[0] . "." . $var[1];

                if ($var == '7.0' || $var == '7.1') {
                    $distid = 'centos70';
                } else {
                    $distid = 'centos72';
                }

            } else {
                $distname = 'Redhat';
                $distver = 'Unknown';
                $distid = 'fedora9';
                $distbaseid = 'fedora';

            }
        } //** Gentoo
        elseif (file_exists('/etc/gentoo-release')) {
            $content = file_get_contents('/etc/gentoo-release');
            preg_match_all('/([0-9]{1,2})/', $content, $version);
            $distname = 'Gentoo';
            $distver = $version[0][0] . $version[0][1];
            $distid = 'gentoo';
            $distbaseid = 'gentoo';

        } else {
            die('Unrecognized GNU/Linux distribution');
        }

        // Set $distconfid to distid, if no different id for the config is defined
        if (!isset($distconfid)) {
            $distconfid = $distid;
        }
        return array(
            'name' => $distname,
            'version' => $distver,
            'id' => $distid,
            'confid' => $distconfid,
            'baseid' => $distbaseid
        );
    }
}