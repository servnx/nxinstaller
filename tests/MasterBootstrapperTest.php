<?php

namespace NxInstaller\Tests;

use NxInstaller\BaseTestCase;
use NxInstaller\Installer\Server\AcceptMinionKey;
use NxInstaller\Installer\Server\AddMasterIP;
use NxInstaller\Installer\Server\InstallSalt;
use NxInstaller\Installer\Utilities\RestartMinion;

class MasterBootstrapperTest extends BaseTestCase
{
    /** @test */
    public function it_installed_salt()
    {
        if (!installed('salt')) {
            (new InstallSalt($this->io, $this->salt))->handle();
        }

        $this->assertTrue(installed('salt'), 'Salt failed to Install!');
    }

    /** @test */
    public function it_added_master_ip_to_minion_config()
    {
        if (!str_contains(file_get_contents('/etc/salt/minion'), 'master: 127.0.0.1')) {
            (new AddMasterIP($this->io, $this->salt))->handle();
        }

        $this->assertTrue(
            str_contains(file_get_contents('/etc/salt/minion'), 'master: 127.0.0.1'),
            "Minion config does NOT contain 'master: 127.0.0.1'!"
        );
    }

    /** @test */
    public function it_restarted_the_minion_services()
    {
        $pids_before = explode("\n", trim(shell_exec('pgrep salt-minion')));

        (new RestartMinion($this->io, $this->salt))->handle();

        $pids_after = explode("\n", trim(shell_exec('pgrep salt-minion')));

        $this->assertTrue(($pids_before !== $pids_after), 'salt-minion services failed to restart!');
    }

    /** @test */
    public function it_accepts_the_given_targets_key()
    {
        if (!in_array(hostname(), $this->salt->getKeys('minions'))) {
            (new AcceptMinionKey($this->io, $this->salt))
                ->setTimeout(15)
                ->handle();
        }

        $this->assertTrue(
            in_array(hostname(), $this->salt->getKeys('minions')),
            hostname() . " is not in 'minions' array"
        );

        // $this->config->get('os::centos::php-fpm-sock_path')
        dd($this->getOsConfig('php-fpm-sock_path'));
    }

// CopySaltResources
// RefreshPillars

}